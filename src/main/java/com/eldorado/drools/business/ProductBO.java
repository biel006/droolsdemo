package com.eldorado.drools.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.eldorado.drools.enumerator.ProductType;
import com.eldorado.drools.exception.BusinessException;
import com.eldorado.drools.exception.ResourceNotFoundException;
import com.eldorado.drools.mechanism.ProductRuleBase;
import com.eldorado.drools.util.DateUtil;
import com.eldorado.drools.vo.Product;

/**
 * This class represent the Business Layer responsible to handle business logic
 * for {@link Product} entities.
 * 
 * @author gabriel.nascimento
 */
@Lazy
@Service("ProductBusiness")
public class ProductBO {
	/**
	 * Method responsible for getting all {@link Product} objects from the
	 * database
	 * 
	 * @return a list with all {@link Product} objects
	 * @throws BusinessException is thrown whenever a business problem occurs
	 */
	public List<Product> getAll() throws BusinessException {
		List<Product> products = createProductList();

		ProductRuleBase.applyRules(products);
		return products;
	}

	/**
	 * Method responsible for getting a {@link Product} by its identifier
	 * 
	 * @param id the entity's identifier
	 * @return a {@link Product} object
	 * @throws BusinessException is thrown whenever a business problem occurs
	 * @throws ResourceNotFoundException is thrown whenever a object with the
	 *             given identifier no longer exists
	 */
	public Product getById(Long id) throws BusinessException, ResourceNotFoundException {
		Product product = null;

		List<Product> products = createProductList();
		for (Product prod : products) {
			if (prod.getId() == id) {
				product = prod;
			}
		}

		if (product == null) {
			throw new ResourceNotFoundException();
		}

		return product;
	}

	private List<Product> createProductList() {
		List<Product> products = new ArrayList<Product>();
		products.add(new Product(1L, "Product A", DateUtil.date(2016, 1, 1), 10.5, ProductType.TYPE_A));
		products.add(new Product(2L, "Product B", DateUtil.date(2016, 1, 2), 20.4, ProductType.TYPE_B));
		products.add(new Product(3L, "Product C", DateUtil.date(2016, 1, 3), 30.3, ProductType.TYPE_C));
		products.add(new Product(4L, "Product D", DateUtil.date(2016, 2, 1), 20.1, ProductType.TYPE_A));
		products.add(new Product(5L, "Product E", DateUtil.date(2016, 2, 2), 10.0, ProductType.TYPE_B));
		products.add(new Product(6L, "Product F", DateUtil.date(2016, 2, 3), 40.6, ProductType.TYPE_C));
		products.add(new Product(7L, "Product G", DateUtil.date(2016, 3, 1), 50.7, ProductType.TYPE_A));
		products.add(new Product(8L, "Product H", DateUtil.date(2016, 3, 2), 60.8, ProductType.TYPE_B));
		products.add(new Product(9L, "Product I", DateUtil.date(2016, 3, 3), 70.9, ProductType.TYPE_C));
		products.add(new Product(10L, "Product J", DateUtil.date(2016, 4, 1), 11.1, ProductType.TYPE_A));
		products.add(new Product(11L, "Product K", DateUtil.date(2016, 4, 2), 22.2, ProductType.TYPE_B));
		products.add(new Product(12L, "Product L", DateUtil.date(2016, 4, 3), 33.3, ProductType.TYPE_C));
		products.add(new Product(13L, "Product M", DateUtil.date(2016, 5, 1), 44.4, ProductType.TYPE_A));
		products.add(new Product(14L, "Product N", DateUtil.date(2016, 5, 2), 55.5, ProductType.TYPE_B));
		products.add(new Product(15L, "Product O", DateUtil.date(2016, 5, 3), 66.6, ProductType.TYPE_C));
		return products;
	}
}
