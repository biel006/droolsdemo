package com.eldorado.drools.mechanism;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import com.eldorado.drools.vo.Product;

public class ProductRuleBase {
	private static KnowledgeBase createKnowledgeBase() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

		kbuilder.add(ResourceFactory.newClassPathResource("product.drl"), ResourceType.DRL);

		handleErrors(kbuilder);

		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

		return kbase;
	}

	private static void handleErrors(KnowledgeBuilder kbuilder) {
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
	}

	public static void applyRules(Iterable<Product> products) {
		KnowledgeBase kbase = createKnowledgeBase();
        StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();

        ksession.insert(products);
        ksession.fireAllRules();
	}
}
