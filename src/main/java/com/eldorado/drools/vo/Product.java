package com.eldorado.drools.vo;

import java.io.Serializable;
import java.util.Date;

import com.eldorado.drools.enumerator.ProductType;

public class Product implements Serializable {
	private static final long serialVersionUID = -3962952719265505547L;

	public Product(Long id, String name, Date buildDate, double price, ProductType type) {
		this.id = id;
		this.name = name;
		this.buildDate = buildDate;
		this.price = price;
		this.type = type;
	}

	private Long id;

	private ProductType type;

	private String name;

	private Date buildDate;

	private double price;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductType getType() {
		return type;
	}

	public void setType(ProductType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBuildDate() {
		return buildDate;
	}

	public void setBuildDate(Date buildDate) {
		this.buildDate = buildDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
