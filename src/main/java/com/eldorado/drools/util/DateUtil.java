package com.eldorado.drools.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	/**
	 * Build a {@link Date} object with a given year, month and day.
	 * <p>
	 * All the parameters are zero based, which means the parameters 2016, 5, 3
	 * will give you the date May/03/2016.
	 * <p>
	 * The result will be floor based, which means it will contains no time
	 * values (hours, minutes, seconds and milliseconds).
	 * 
	 * @param year the year value
	 * @param month the month value
	 * @param day the day value
	 * @return a {@link Date} object
	 */
	public static Date date(int year, int month, int day) {
		Calendar calendar = GregorianCalendar.getInstance();
		// we are subtracting 1 here because the set month method is zero based
		calendar.set(year, (month - 1), day, 0, 0, 0);
		return calendar.getTime();
	}

	/**
	 * Method responsible for getting the floor of a given date.
	 * <p>
	 * The date to be returned will contains only the year, month, and day from
	 * the given date without the time values (hours, minutes, seconds and
	 * milliseconds).
	 * 
	 * @param date the {@link Date} object which contains the year, month and
	 *            day values
	 * @return a {@link Date} object with no time information
	 */
	public static Date getFloor(Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * Method responsible for getting the ceiling of a given date.
	 * <p>
	 * The date to be returned will contains the same year, month and day of the
	 * given date, although, the time will be set as 23:59:59.999.
	 * 
	 * @param date the {@link Date} object which contains the year, month and
	 *            day values
	 * @return a {@link Date} object with no time information
	 */
	public static Date getCeiling(Date date) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	/**
	 * Add one day to a given {@link Date} object
	 * 
	 * @param date our base date
	 * @return a new {@link Date} object
	 */
	public static Date addDay(Date date) {
		return addDay(date, 1);
	}

	/**
	 * Add a number of days to a given {@link Date} object
	 * 
	 * @param date our base date
	 * @param day the number of days to be added
	 * @return a new {@link Date} object
	 */
	public static Date addDay(Date date, int day) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		return calendar.getTime();
	}

	/**
	 * Add one month to a given {@link Date} object
	 * 
	 * @param date our base date
	 * @return a new {@link Date} object
	 */
	public static Date addMonth(Date date) {
		return addMonth(date, 1);
	}

	/**
	 * Add a number of months to a given {@link Date} object
	 * 
	 * @param date our base date
	 * @param month the number of months to be added
	 * @return a new {@link Date} object
	 */
	public static Date addMonth(Date date, int month) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		return calendar.getTime();
	}

	/**
	 * Add one year to a given {@link Date} object
	 * 
	 * @param date our base date
	 * @return a new {@link Date} object
	 */
	public static Date addYear(Date date) {
		return addYear(date, 1);
	}

	/**
	 * Add a number of years to a given {@link Date} object
	 * 
	 * @param date our base date
	 * @param year the number of years to be added
	 * @return a new {@link Date} object
	 */
	public static Date addYear(Date date, int year) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, year);
		return calendar.getTime();
	}
}
