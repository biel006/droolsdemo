package com.eldorado.drools.exception;

import org.springframework.http.HttpStatus;

/**
 * This interface represents a HTTP Resource Exception.
 * <p>
 * By implementing it, the class is telling that he has a HTTP status associated
 * to it.
 *
 * @author gabriel.nascimento
 */
public interface ResourceException {
	/**
	 * Method responsible for getting the @{link HttpStatus} of this Exception
	 * 
	 * @return a {@link HttpStatus}
	 */
	public HttpStatus getStatus();
}
