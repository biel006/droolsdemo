package com.eldorado.drools.exception;

import org.springframework.http.HttpStatus;

/**
 * This class represents a Business Exception.
 * <p>
 * It is generally thrown every time we have a problem on the BO layer.
 * 
 * @author gabriel.nascimento
 */
public class ResourceNotFoundException extends Exception implements ResourceException {
	private static final long serialVersionUID = 5164326806410792176L;
	private static final HttpStatus status = HttpStatus.NOT_FOUND;

	public ResourceNotFoundException() {
		super();
	}

	public ResourceNotFoundException(String msg) {
		super(msg);
	}

	public ResourceNotFoundException(Exception ex) {
		super(ex);
	}

	public ResourceNotFoundException(String msg, Exception ex) {
		super(msg, ex);
	}

	/*
	 * @see com.motorola.motopoc.exception.ResourceException#getStatus()
	 */
	@Override
	public HttpStatus getStatus() {
		return status;
	}
}
