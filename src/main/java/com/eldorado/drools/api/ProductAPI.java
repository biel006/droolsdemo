package com.eldorado.drools.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eldorado.drools.business.ProductBO;
import com.eldorado.drools.exception.BusinessException;
import com.eldorado.drools.exception.ResourceNotFoundException;
import com.eldorado.drools.vo.Product;

/**
 * This class represents the API responsible to handle {@link Product} REST calls
 * 
 * @author gabriel.nascimento
 */
@Lazy
@CrossOrigin
@RestController("ProductService")
@RequestMapping("/product")
@SuppressWarnings("rawtypes")
public class ProductAPI extends RestService {
	@Autowired
	private ProductBO business;

	/**
	 * This service is responsible for listing all {@link Product} entities.
	 * <p>
	 * The response will be wrapped into a {@link ResponseEntity} object which
	 * contains the request result and the HTTP status.<br>
	 * We can expect the following results:
	 * <p>
	 * 200 - In case everything was ok<br>
	 * 500 - In case something went wrong with the business layer
	 * 
	 * @return a list with all saved {@link Product}s
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity list() {
		ResponseEntity response = null;
		try {
			response = ok(business.getAll());
		} catch (BusinessException ex) {
			response = response(ex.getStatus(), "Something went wrong. Try again in a few seconds!" + ex.getMessage());
		} catch (Exception ex) {
			response = internalServerError();
		}
		return response;
	}

	/**
	 * This service is responsible for getting a {@link Product} entity.
	 * <p>
	 * The response will be wrapped into a {@link ResponseEntity} object which
	 * contains the request result and the HTTP status.<br>
	 * We can expect the following results:
	 * <p>
	 * 200 - In case everything was ok<br>
	 * 500 - In case something went wrong with the business layer<br>
	 * 404 - In case the requested resource was not found
	 * 
	 * @param id the {@link Product}'s identifier
	 * @return a {@link Product} object
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity get(@PathVariable Long id) {
		ResponseEntity response = null;
		try {
			response = ok(business.getById(id));
		} catch (BusinessException ex) {
			response = response(ex.getStatus(), "Something went wrong. Try again in a few seconds!");
		} catch (ResourceNotFoundException ex) {
			response = response(ex.getStatus(), "There are no entities with the identifier " + id);
		} catch (Exception ex) {
			response = internalServerError();
		}
		return response;
	}
}
