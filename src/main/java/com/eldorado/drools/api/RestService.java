package com.eldorado.drools.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * This abstract class should be extended by every REST Service class.
 * <p>
 * It allows the service to create proper responses based on the HTTP status.
 * 
 * @author gabriel.nascimento
 */
@SuppressWarnings("rawtypes")
public abstract class RestService {
	/**
	 * Create a {@link ResponseEntity}.
	 * <p>
	 * It will contains a 200 HTTP Status (OK) and no message on its body
	 * 
	 * @return a {@link ResponseEntity}
	 */
	protected ResponseEntity ok() {
		return ResponseEntity.ok("{\"mes\":\"OK\"}");
	}

	/**
	 * Create a {@link ResponseEntity}.
	 * <p>
	 * It will contains a 200 HTTP Status (OK) and a given object on its body.
	 * 
	 * @param obj object to be set on the response body.
	 * @return a {@link ResponseEntity}
	 */
	protected ResponseEntity ok(Object obj) {
		return ResponseEntity.ok(obj);
	}

	/**
	 * Create a {@link ResponseEntity}.
	 * <p>
	 * It will contains a 500 HTTP Status (Internal Server Error)
	 * 
	 * @return a {@link ResponseEntity}
	 */
	protected ResponseEntity internalServerError() {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body("{\"mes\":\"Something went wrong. contact the system's administrator for more information.\"}");
	}

	/**
	 * Create a {@link ResponseEntity}.
	 * <p>
	 * It will contains a given HTTP status and an object to be set on its body.
	 * 
	 * @param status the HTTP status code to be set.
	 * @param obj object to be set on the response body.
	 * @return a {@link ResponseEntity}
	 */
	protected ResponseEntity response(HttpStatus status, Object obj) {
		return ResponseEntity.status(status).body(obj);
	}
}
