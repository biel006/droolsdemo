(function(){
    'use strict';

    // application module
    angular.module('demo', ['demo.core', 'demo.common', 'demo.external']);

    // external dependencies
    angular.module('demo.external', ['ui.bootstrap', 'toaster', 'cgBusy', 'ngCookies', 'ngMaterial',
                            'ngAnimate', 'ngSanitize', 'ngRoute', 'pascalprecht.translate']);

    // common modules definition
    angular.module('demo.common', ['demo.constants', 'demo.common.directives', 'demo.common.filters', 'demo.common.mechanisms', 'demo.common.security']);
    angular.module('demo.common.directives', []);
    angular.module('demo.common.filters', []);
    angular.module('demo.common.mechanisms', []);
    angular.module('demo.common.security', []);
    
    // core modules definition
    angular.module('demo.core', ['demo.core.store', 'demo.core.products', 'demo.core.rules']);
    angular.module('demo.core.store', []);
    angular.module('demo.core.products', []);
    angular.module('demo.core.rules', []);

    // configure cgBusy component for application
    angular.module('demo').value('cgBusyDefaults',{message:'Please wait', backdrop: true, delay: 0, minDuration: 500});
    
    // configure translate provider component to set the preffered language
    angular.module('demo').config(['$translateProvider', function($translateProvider){
        // Register a loader for the static files. So, the module will search missing translation tables under the specified urls.
        // Those urls are [prefix][langKey][suffix].
        $translateProvider.useStaticFilesLoader({
            prefix: 'assets/translations/',
            suffix: '.json'
        });

        // define sanitization strategy in order to avoid security issues
        $translateProvider.useSanitizeValueStrategy('sanitize');

        // try to determine preferred language
        $translateProvider.determinePreferredLanguage();

        // if the browser is configured in a non supported language, define a fallback
        $translateProvider.fallbackLanguage('en_US');
    }]);
})();
