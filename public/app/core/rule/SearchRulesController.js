(function(){
    'use strict';
    
    /**
     * Controller responsible to handle functionalites related to the rules search UI
     * 
     * @param {!angular.Scope} $scope Angular scope service
     * @param {Angular-UI-Modal} $modal angular-ui wrapper for bootstrap modal
     * @param {AngularJS-Toaster} toaster Angular thirty party module
     * @param {!angular.$filter} $filter Angular $filter service
     * @param {!angular.$location} $location Angular $location wrapper
     * @param {!demo.core.rules.RuleServices} RuleServices application rule services
     * @param {!demo.constants.PaginationConstants} PaginationConstants which contains the application's constants
     */
    angular.module('demo.core.rules').controller('SearchRulesController', SearchRulesController);
    
    // Dependency Injection
    SearchRulesController.$inject = ['$scope', '$modal', 'toaster','$filter','$location', 'RuleServices', 'PaginationConstants'];
   
    // Controller Implementation
    function SearchRulesController($scope, $modal, toaster, $filter, $location, RuleServices,  PaginationConstants){
        // initialize all variables need to put this working
        $scope.ruleList = [];
        $scope.promise = undefined;
        
        // Pagination and search variables
        $scope.preparedRuleList = [];
        $scope.ruleNameToSearch = '';
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = PaginationConstants.ITEMS_PER_PAGE;
        $scope.maxSize = PaginationConstants.MAX_SIZE;
        
        /**
         * Function responsible for preparing the rule list to show it in the paginated table
         * It will - filter the ruleList by the search field
         * 		   - order the result of the filter by partnerName
         *         - return the sliced result to show it paginated
         * @returns {list} filtered and paginated list of rules
         */
        $scope.getPreparedRuleList = function(){
            $scope.preparedRuleList = $filter('filter')($scope.ruleList, {name: $scope.ruleNameToSearch});
            $scope.preparedRuleList = $filter('orderBy')($scope.preparedRuleList, 'name');
    
            $scope.totalItems = $scope.preparedRuleList.length;  
                
            return $scope.preparedRuleList.slice(($scope.currentPage-1) * $scope.itemsPerPage, (($scope.currentPage-1) * $scope.itemsPerPage) + $scope.itemsPerPage);
        };

        /**
         * Function responsible for getting all rules
         */
        $scope.getAllRules = function() {
            // call the get all rules method from service
            $scope.promise = RuleServices.getAllRules();

            //callback in case of success to retrieve the rule list
            var successCallback = function(response) {
                // get answer from response
                $scope.ruleList = response.data.data.results;
            };

            //callback in case of failure to fetch the list of rules
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('searchRules.errorMessage.loadRuleInformationFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
        };

        /**
         * Function responsible for redirecting user to edit a rule
         * @param {String} id rule identifier to be edited
         */
        $scope.editRule = function (id) {
            // redirect user to rule edition
            $location.path( '/rules/' + id + '/manage');
        };

        /**
         * Function responsible for deleting a given rule
         * @param {String} id rule identifier to be edited
         */
        $scope.deleteRule = function (id) {
            // call the get all rules method from service
            $scope.promise = RuleServices.deleteRule(id);

            //callback in case of success to retrieve the rule list
            var successCallback = function(response) {
                // removing item from list
                $scope.ruleList = $scope.ruleList.filter(i => i.id !== id);
            };

            //callback in case of failure to fetch the list of rules
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('searchRules.errorMessage.deleteFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
        };
    }  
})();

