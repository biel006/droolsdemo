(function(){
	'use strict';
	/**
	 * This template is used for creating app.constants.js, this file is used for creating the module demo.constants
	 */
	angular.module('demo.constants',[])
		.constant('BASE_URL','//localhost:8000/')
		.constant('REQUEST_TIMEOUT', 20000);
	angular.module('demo.constants').value('PaginationConstants', {MAX_SIZE: 5, ITEMS_PER_PAGE: 10});
})();