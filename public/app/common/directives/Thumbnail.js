(function(){
    'use strict';

    /**
     * Module responsible for read text a file content as soon as they were selected by browser
     *
     * @param {angular.$parse} $parse Converts Angular expression into a function.
     */
     angular.module('demo.common.directives').directive('thumbnail', Thumbnail);
     function Thumbnail($modal) {
        return {
			template: '<img ng-src="{{src}}/portrait_small.jpg" ng-click="onThumbnailClick()"/>',
            restrict: 'E',
            scope: {
                'src': '='
            },
            link: function(scope) {
                scope.onThumbnailClick = function() {
					scope.modalInstance = $modal.open({
                        animation: true,
                        scope: scope,
                        size: 'sm',
                        templateUrl: 'common/templates/thumbnail-modal.html'
                    });
				};
		  }
       };
    }
})();