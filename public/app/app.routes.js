(function () {
    'use strict';
    // define application routes
    angular.module('demo').config(['$routeProvider', function ($routeProvider) {
            $routeProvider
                // Store Route
                .when('/store', {
                    templateUrl: "core/store/storeView.html",
                    controller : "StoreController"
                })

                // Product Routes
                .when('/products', {
                    templateUrl: "core/product/searchProductsView.html",
                    controller : "SearchProductsController"
                })
                .when('/products/:id/manage', {
                    templateUrl: "core/product/productDetailsView.html",
                    controller : "ProductDetailsController"
                })
                .when('/products/create', {
                    templateUrl: "core/product/productDetailsView.html",
                    controller : "ProductDetailsController"
                })

                // Rule Routes
                .when('/rules', {
                    templateUrl: "core/rule/searchRulesView.html",
                    controller : "SearchRulesController"
                })
                .when('/rules/:id/manage', {
                    templateUrl: "core/rule/ruleDetailsView.html",
                    controller : "RuleDetailsController"
                })
                .when('/rules/create', {
                    templateUrl: "core/rule/ruleDetailsView.html",
                    controller : "RuleDetailsController"
                })

                // Otherwise, we should redirect the user to home page
                .otherwise({
                        redirectTo : '/'
                });
            }]);
})();
