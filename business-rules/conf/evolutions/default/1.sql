# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table product (
  id                        bigint not null,
  name                      varchar(255) not null,
  description               varchar(255) not null,
  price                     double not null,
  available                 boolean not null,
  constraint uq_product_name unique (name),
  constraint uq_product_description unique (description),
  constraint pk_product primary key (id))
;

create table rule (
  id                        bigint not null,
  name                      varchar(255) not null,
  dslr                      TEXT not null,
  constraint uq_rule_name unique (name),
  constraint pk_rule primary key (id))
;

create sequence product_seq;

create sequence rule_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists product;

drop table if exists rule;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists product_seq;

drop sequence if exists rule_seq;

