package model.dao;

import java.util.ArrayList;
import java.util.List;
import model.vo.Rule;
import exception.PersistenceException;

public class RuleDAO
{

	/**
	 * Method responsible for inserting a new BusinessRule into database
	 * 
	 * @param rule BusinessRule to be inserted
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public void insert(Rule rule) throws PersistenceException
	{
		try
		{
			rule.save();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}
	}
	
	/**
	 * Method responsible for delete BusinessRule into database
	 * 
	 * @param ruleId BusinessRule Id to be delete
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public void delete(Long ruleId) throws PersistenceException
	{
		try
		{
			Rule.find.byId(ruleId).delete();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}
	}
	
	/**
	 * Method responsible for update BusinessRule into database
	 * 
	 * @param rule BusinessRule  to be update
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public void update(Rule rule) throws PersistenceException
	{
		
		try
		{
			rule.update();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}
	}
	
	/**
	 * Method responsible for returning a list of all business rules
	 * 
	 * @return list of all business rules
	 * @throws PersistenceException in case of any failure during process
	 */
	public List<Rule> getAllRules() throws PersistenceException
	{
		List<Rule> rulesToBeReturned = new ArrayList<Rule>();

		try
		{
			rulesToBeReturned = Rule.find.findList();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}

		return rulesToBeReturned;
	}

	/**
	 * Method responsible for returning a business rule with the given id
	 * 
	 * @return business rule with given id
	 * @throws PersistenceException in case of any failure during process
	 */
	public Rule getRuleById(long id) throws PersistenceException
	{
		Rule ruleToBeReturned = null;

		try
		{
			ruleToBeReturned = Rule.find.where().eq("id", id).findUnique();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}

		return ruleToBeReturned;
	}

	/**
	 * Method responsible for returning a business rule with the given name
	 * 
	 * @return business rule with given name
	 * @throws PersistenceException in case of any failure during process
	 */
	public Rule getRuleByName(String name) throws PersistenceException
	{
		Rule ruleToBeReturned = null;

		try
		{
			ruleToBeReturned = Rule.find.where().eq("name", name).findUnique();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}

		return ruleToBeReturned;
	}

	/**
	 * Method responsible for returning a list of all business rules with given product and partner IDs
	 * 
	 * @return list of business rules with given product and partner IDs
	 * @throws PersistenceException in case of any failure during process
	 */
	public List<Rule> listRulesByProductAndByPartner(Long productId, Long partnerId) throws PersistenceException
	{
		List<Rule> rulesToBeReturned = new ArrayList<Rule>();

		try
		{
			rulesToBeReturned = Rule.find.where().eq("product_id",productId).eq("partner_id",partnerId).findList();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}

		return rulesToBeReturned;
	}
}
