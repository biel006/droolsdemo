package model.dao;

import java.util.ArrayList;
import java.util.List;

import model.vo.Product;
import exception.PersistenceException;

public class ProductDAO
{
	
	/**
	 * Method responsible for returning a product using name as condition
	 * 
	 * @param name name to be used as filter
	 * @return product in according to the filter
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public Product selectByProductName(String name) throws PersistenceException
	{
		Product productToBeReturned = null;

		try
		{
			productToBeReturned = Product.find.where().eq("name", name).findUnique();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}

		return productToBeReturned;
	}
	
	/**
	 * Method responsible for returning a product using id as condition
	 * 
	 * @param id productId identifier to be used as filter
	 * @return product in according to the filter
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public Product selectByProductId(long id) throws PersistenceException
	{
		Product productToBeReturned = null;

		try
		{
			productToBeReturned = Product.find.where().eq("id", id).findUnique();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}

		return productToBeReturned;
	}

	/**
	 * Method responsible for inserting a new product into database
	 * 
	 * @param newProduct product to be inserted
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public void insert(Product newProduct) throws PersistenceException
	{
		try
		{
			newProduct.save();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}
	}

	/**
	 * Method responsible for updating a product into database
	 * 
	 * @param product product to be updated
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public void update(Product product) throws PersistenceException
	{
		try
		{
			product.update();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}
	}

	/**
	 * Method responsible for returning all products
	 * 
	 * @return all existent products
	 * @throws PersistenceException in case of any failure during persistence process
	 */
	public List<Product> getAllProducts() throws PersistenceException
	{
		List<Product> productsList = new ArrayList<Product>();

		try
		{
			productsList = Product.find.findList();
		}
		catch (Exception ex)
		{
			throw new PersistenceException(ex);
		}

		return productsList;
	}
}
