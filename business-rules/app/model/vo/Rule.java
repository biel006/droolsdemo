package model.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import play.db.ebean.Model;

@Entity
@SuppressWarnings("serial")
public class Rule extends Model
{

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false, unique = true)
	private String name;

	@Column(nullable = false, unique = false, columnDefinition = "TEXT")
	private String dslr;

	public static Finder<Long, Rule> find = new Finder<Long, Rule>(Long.class, Rule.class);

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDslr()
	{
		return dslr;
	}

	public void setDslr(String rule)
	{
		this.dslr = rule;
	}

	public static Finder<Long, Rule> getFind()
	{
		return find;
	}

	public static void setFind(Finder<Long, Rule> find)
	{
		Rule.find = find;
	}
}
