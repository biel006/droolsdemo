package model.services;

import java.util.List;

import mechanism.drools.RulesEngine;
import model.dao.RuleDAO;
import model.vo.Rule;
import exception.BusinessException;
import exception.PersistenceException;
import exception.RulesEngineException;

public class RuleServices
{
	/**
	 * Method responsible for creating a new BusinessRule
	 *
	 * @param rule BusinessRule to be created
	 * @throws BusinessException exception in case of something wrong happening during operation
	 * @throws RulesEngineException in case of something wrong during rule validation
	 */
	public void createRule(Rule rule) throws BusinessException, RulesEngineException
	{
		try
		{
			// Validate the rule using the RulesEngine
			RulesEngine.getInstance().validateRule(rule);

			// insert the rule in the database
			RuleDAO ruleDAO = new RuleDAO();
			ruleDAO.insert(rule);
			RulesEngine.getInstance().buildKnowledgeBase();
		}
		catch (PersistenceException e)
		{
			throw new BusinessException(e);
		}
	}

	/**
	 * Method responsible for delete a Rule
	 *
	 * @param ruleId BusinessRule Id to be delete
	 * @throws BusinessException exception in case of happening something wrong during operation
	 */
	public void deleteRule(Long ruleId) throws BusinessException
	{
		try
		{
			// deletes the rule from the database
			RuleDAO ruleDAO = new RuleDAO();
			ruleDAO.delete(ruleId);
			RulesEngine.getInstance().buildKnowledgeBase();

		}
		catch (PersistenceException e)
		{
			throw new BusinessException(e);
		}
	}

	/**
	 * Method responsible for updating a BusinessRule
	 *
	 * @param rule BusinessRule to be updated
	 * @throws BusinessException exception in case of happening something wrong during operation
	 */
	public void updateRule(Rule rule) throws BusinessException, RulesEngineException
	{
		try
		{
			// Validate the rule using the RulesEngine
			RulesEngine.getInstance().validateRule(rule);

			// update the rule in the database
			RuleDAO ruleDAO = new RuleDAO();
			ruleDAO.update(rule);
			RulesEngine.getInstance().buildKnowledgeBase();

			
		}
		catch (PersistenceException e)
		{
			throw new BusinessException(e);
		}
	}

	/**
	 * Method responsible for returning a BusinessRule using name as condition
	 *
	 * @param name name to be used as filter
	 * @return BusinessRule in according to the filter
	 * @throws BusinessException exception in case of happening something wrong during operation
	 */
	public Rule getRuleByName(String name) throws BusinessException
	{
		// rule to be returned
		Rule businessRule = null;

		try
		{
			// gets the rule by it's name
			RuleDAO ruleDAO = new RuleDAO();
			businessRule = ruleDAO.getRuleByName(name);
		}
		catch (PersistenceException e)
		{
			throw new BusinessException(e);
		}

		return businessRule;
	}

	/**
	 * Method responsible for returning a BusinessRule using ID as condition
	 *
	 * @param id ID to be used as filter
	 * @return BusinessRule in according to the filter
	 * @throws BusinessException exception in case of happening something wrong during operation
	 */
	public Rule getRuleById(Long id) throws BusinessException
	{
		// rule to be returned
		Rule businessRule = null;

		try
		{
			// gets the rule by it's ID
			RuleDAO ruleDAO = new RuleDAO();
			businessRule = ruleDAO.getRuleById(id);
		}
		catch (PersistenceException e)
		{
			throw new BusinessException(e);
		}

		return businessRule;
	}

	/**
	 * Method responsible for returning a List of all BusinessRules
	 *
	 * @return List<BusinessRule> in according to the filter
	 * @throws BusinessException exception in case of happening something wrong during operation
	 */
	public List<Rule> getAllRules() throws BusinessException
	{
		// list of all rules to be returned
		List<Rule> businessRulesList = null;

		try
		{
			// gets the list of all rules
			RuleDAO ruleDAO = new RuleDAO();
			businessRulesList = ruleDAO.getAllRules();
		}
		catch (PersistenceException e)
		{
			throw new BusinessException(e);
		}

		return businessRulesList;
	}
}
