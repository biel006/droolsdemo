package model.services;

import java.util.HashMap;
import java.util.List;

import model.dao.ProductDAO;
import model.vo.Product;
import exception.BusinessException;
import exception.PersistenceException;
import mechanism.drools.RulesEngine;

public class ProductServices
{

	/**
	 * Method responsible for inserting a new product into database
	 * 
	 * @param newProduct product to be inserted
	 * @throws BusinessException in case of any failure during inserting process
	 */
	public void insert(Product newProduct) throws BusinessException
	{
		try
		{
			ProductDAO productDao = new ProductDAO();
			productDao.insert(newProduct);
		}
		catch (PersistenceException ex)
		{
			throw new BusinessException(ex);
		}
	}

	/**
	 * Method responsible for updating a product into database
	 * 
	 * @param product product to be updated
	 * @throws BusinessException in case of any failure during updating process
	 */
	public void update(Product product) throws BusinessException
	{
		try
		{
			ProductDAO productDao = new ProductDAO();
			productDao.update(product);
		}
		catch (PersistenceException ex)
		{
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * Method responsible for returning a list of all products in database
	 * 
	 * @return List of all Products
	 * @throws BusinessException in case of any problems during listing process
	 */
	public List<Product> listAllProductsWithRules() throws BusinessException
	{
		// list of all products to be returned
		List<Product> productListToBeReturned = null;

		try
		{
			// Initiates the product DAO and retrieves the list of all products or throws an exception if any error occur
			ProductDAO productDAO = new ProductDAO();
			productListToBeReturned = productDAO.getAllProducts();
			
			for (Product product : productListToBeReturned) {
				HashMap<String, Object> facts = new HashMap<>();
				facts.put("product", product);

				RulesEngine.getInstance().applyRules(facts, "product");
			}

		}
		catch (PersistenceException pe)
		{
			throw new BusinessException(pe);
		}

		return productListToBeReturned;
	}

	/**
	 * Method responsible for returning a list of all products in database
	 * 
	 * @return List of all Products
	 * @throws BusinessException in case of any problems during listing process
	 */
	public List<Product> listAllProducts() throws BusinessException
	{
		// list of all products to be returned
		List<Product> productListToBeReturned = null;

		try
		{
			// Initiates the product DAO and retrieves the list of all products or throws an exception if any error occur
			ProductDAO productDAO = new ProductDAO();
			productListToBeReturned = productDAO.getAllProducts();

		}
		catch (PersistenceException pe)
		{
			throw new BusinessException(pe);
		}

		return productListToBeReturned;
	}

	/**
	 * Method responsible for returning a product filter by id
	 * 
	 * @param id id of the product
	 * @return Product identified by the given id
	 * @throws BusinessException in case of any problems during listing process
	 */
	public Product getProductById(long id) throws BusinessException
	{
		// product to be returned
		Product productToBeReturned = null;

		try
		{
			// Initiates the product DAO and retrieves the product by it's ID or throws an exception if any error occur
			ProductDAO productDAO = new ProductDAO();
			productToBeReturned = productDAO.selectByProductId(id);
		}
		catch (PersistenceException pe)
		{
			throw new BusinessException(pe);
		}

		return productToBeReturned;
	}

	/**
	 * Method responsible for returning a product filter by name
	 * 
	 * @param name name of the product
	 * @return Product identified by the given id
	 * @throws BusinessException in case of any problems during listing process
	 */
	public Product getProductByName(String name) throws BusinessException
	{
		// product to be returned
		Product productToBeReturned = null;

		try
		{
			ProductDAO productDAO = new ProductDAO();
			productToBeReturned = productDAO.selectByProductName(name);
		}
		catch (PersistenceException pe)
		{
			throw new BusinessException(pe);
		}

		return productToBeReturned;
	}
}
