package exception;

import play.mvc.Controller;
import play.mvc.Results.Status;

/**
 * This exception must be thrown when the partner does not exists.
 */
@SuppressWarnings("serial")
public class RuleNotFoundException extends RuntimeException implements HttpConvertibleException {

	/**
	 * Instantiates a new rule not found exception.
	 *
	 * @param message
	 *            the message
	 */
	public RuleNotFoundException(String message) {
		super(message);
	}

	/**
	 * 
	 * @see exception.HttpConvertibleException#getHttpErrorStatus()
	 */
	@Override
	public Status getHttpErrorStatus() {
		return Controller.notFound(getMessage());
	}
}
