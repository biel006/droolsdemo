package exception;

import mechanism.drools.RulesEngine;
import play.mvc.Controller;
import play.mvc.Results.Status;

/**
 * This exception must be thrown when a problem occurs during a process
 * performed by the {@link RulesEngine}
 */
@SuppressWarnings("serial")
public class RulesEngineException extends RuntimeException implements HttpConvertibleException {

	/** The error status. */
	private final transient Status errorStatus;

	/**
	 * Class constructor
	 *
	 * @param message
	 *            the message
	 * @see Exception#Exception()
	 */
	public RulesEngineException(String message) {
		super(message);
		errorStatus = Controller.status(409, message);
	}

	/**
	 * Class constructor
	 *
	 * @param ex
	 *            The root exception
	 * @see Exception#Exception(Throwable)
	 */
	public RulesEngineException(Throwable ex) {
		super(ex);
		errorStatus = Controller.status(409, ex.getMessage());
	}

	/**
	 * Class constructor
	 *
	 * @param errorStatus
	 *            The HTTP error status to be thrown
	 * @param e
	 *            The root exception
	 */
	public RulesEngineException(Status errorStatus, Throwable e) {
		super(e);
		this.errorStatus = errorStatus;
	}

	/**
	 * Gets the http error status.
	 *
	 * @return the http error status
	 */
	@Override
	public Status getHttpErrorStatus() {
		return errorStatus;
	}
}