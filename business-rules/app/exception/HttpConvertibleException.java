package exception;

import play.mvc.Results.Status;

/**
 * All exceptions implementing this interface can be converted to an HTTP
 * response code
 *
 */
public interface HttpConvertibleException {
	/**
	 * 
	 * Returns the HTTP response code assigned to the exception
	 * 
	 * @return {@link Status} The corresponding HTTP response code
	 */
	Status getHttpErrorStatus();
}
