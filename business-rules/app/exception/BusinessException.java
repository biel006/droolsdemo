package exception;

public class BusinessException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2976357868337737835L;

	/**
	 * @see Exception#Exception()
	 */
	public BusinessException()
	{
		super();
	}
	
	/**
	 * @see Exception#Exception()
	 */
	public BusinessException(String message)
	{
		super(message);
	}

	/**
	 * @see Exception#Exception(Throwable)
	 */
	public BusinessException(Throwable ex)
	{
		super(ex);
	}
}
