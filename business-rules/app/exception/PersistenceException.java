package exception;

public class PersistenceException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1486126310174403389L;

	/**
	 * @see Exception#Exception(Throwable)
	 */
	public PersistenceException(Throwable ex)
	{
		super(ex);
	}
}
