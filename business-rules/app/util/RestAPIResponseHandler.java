package util;

import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;

import model.vo.Product;
import model.vo.Rule;
import play.libs.Json;


public class RestAPIResponseHandler
{
	/**
	 * Method responsible for parsing a products list and generating an equivalent JSON list.
	 * 
	 * @param products list of products to be converted
	 * @return JSON with all products
	 */
	public static String createJsonForProductsList(List<Product> products)
	{
		String productsJson = new String("");

		productsJson += "[";
		for (Product product : products)
		{
			productsJson += "{";
			productsJson += "\"id\": " + product.getId() + ", ";
			productsJson += "\"price\": " + product.getPrice() + ", ";
			productsJson += "\"name\": " + "\"" + product.getName() + "\",";
			productsJson += "\"description\": " + "\"" + product.getDescription() + "\",";
			productsJson += "\"available\": " + "\"" + product.isAvailable() + "\"";
			productsJson += "},";
		}
		productsJson = productsJson.substring(0, productsJson.length() - 1);
		productsJson += "]";

		return productsJson;
	}
	
	/**
	 * Method responsible for parsing a product and generating an equivalent JSON.
	 * 
	 * @param product to be converted
	 * @return JSON with product
	 */
	public static String createJsonForProduct(Product product)
	{
		String productsJson = new String("");

		productsJson += "{";
		productsJson += "\"id\": " + product.getId() + ", ";
		productsJson += "\"price\": " + product.getPrice() + ", ";
		productsJson += "\"name\": " + "\"" + product.getName() + "\",";
		productsJson += "\"description\": " + "\"" + product.getDescription() + "\",";
		productsJson += "\"available\": " + "\"" + product.isAvailable() + "\"";
		productsJson += "}";

		return productsJson;
	}


	/**
	 * Method responsible for parsing a BusinessRule and generating an equivalent JSON.
	 * 
	 * @param Rule object to be converted
	 * @return JSON representing the rule
	 */
	public static String createJsonForBusinessRule(Rule rule)
	{
		ObjectNode ruleNode = Json.newObject();
		
		ruleNode.put("id", rule.getId());
		ruleNode.put("name", rule.getName());
		ruleNode.put("dslr", rule.getDslr());
		
		return ruleNode.toString();
	}

	/**
	 * Method responsible for parsing a BusinessRule list and generating an equivalent JSON.
	 * 
	 * @param Rule object to be converted
	 * @return JSON representing the rules list
	 */
	public static String createJsonForBusinessRuleList(List<Rule> rules)
	{
		StringBuilder jsonResult = new StringBuilder();
		jsonResult.append("{\"rules\":[");
		boolean first = true;
		for (Rule rule : rules)
		{
			jsonResult.append(first ? "" : ",");
			
			ObjectNode ruleNode = Json.newObject();
			
			ruleNode.put("id", rule.getId());
			ruleNode.put("name", rule.getName());
			ruleNode.put("dslr", rule.getDslr());
			
			jsonResult.append(ruleNode.toString());
			first = false;
		}
		jsonResult.append("]}");
		return jsonResult.toString();
	}
}
