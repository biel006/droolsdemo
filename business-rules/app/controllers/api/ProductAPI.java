package controllers.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import exception.BusinessException;
import model.services.ProductServices;
import model.vo.Product;
import play.mvc.Controller;
import play.mvc.Result;
import util.RestAPIResponseHandler;

public class ProductAPI extends Controller
{
	public static Result create() {
		
		ProductServices productServices = new ProductServices();
		Product productToCreated;
		
		// retrieves the Json from the request
		JsonNode jsonRequest = request().body().asJson();

		// extracts the Product from the Json
		try {
			
			productToCreated = extractProductFromJson(jsonRequest);
			productServices.insert(productToCreated);
			
		} catch (BusinessException e) {
			return internalServerError("Error:" + e.getMessage());	
		}
		
		return ok("Product Created with successs");
	}
	
	public static Result update(Long id) {
		
		ProductServices productServices = new ProductServices();
		Product productToCreated;
		
		// retrieves the Json from the request
		JsonNode jsonRequest = request().body().asJson();

		// extracts the Product from the Json
		try {
			
			productToCreated = extractProductFromJson(jsonRequest);
			productServices.update(productToCreated);
			
		} catch (BusinessException e) {
			return internalServerError("Error:" + e.getMessage());	
		}
		
		return ok("Product Update with successs");
	}

	public static Result getById(Long id) {
		
		Status methodStatus = null;
		ProductServices productServices = new ProductServices();
		Product productToReturn;
		
		try {
			productToReturn = productServices.getProductById(id);
			methodStatus = ok(RestAPIResponseHandler.createJsonForProduct(productToReturn));
		} catch (BusinessException e) {
			methodStatus = internalServerError("Error" + e.getMessage());
		}
		
		return methodStatus;
	}
	
	/**
	 * Method used to get all products from the database
	 * 
	 * @return response with a list of all products as JSON object
	 */
	public static Result getProductsToSell()
	{
		Status methodStatus = ok();
		List<Product> productsToBeReturned = new ArrayList<Product>();
		ProductServices productServices = new ProductServices();

		try
		{
			productsToBeReturned = productServices.listAllProductsWithRules();

			if (productsToBeReturned != null)
			{
				methodStatus = ok(RestAPIResponseHandler.createJsonForProductsList(productsToBeReturned));
			}
			else
			{
				methodStatus = badRequest("No products found.");
			}
		}
		catch (Exception e)
		{
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}

	
	/**
	 * Method used to get all products from the database
	 * 
	 * @return response with a list of all products as JSON object
	 */
	public static Result getAll()
	{
		Status methodStatus = ok();
		List<Product> productsToBeReturned = new ArrayList<Product>();
		ProductServices productServices = new ProductServices();

		try
		{
			productsToBeReturned = productServices.listAllProducts();

			if (productsToBeReturned != null)
			{
				methodStatus = ok(RestAPIResponseHandler.createJsonForProductsList(productsToBeReturned));
			}
			else
			{
				methodStatus = badRequest("No products found.");
			}
		}
		catch (Exception e)
		{
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}

	/**
	 * Method used to populate database with hard coded mock data
	 * 
	 * @return response with HTTP status code indicating success (200) or failure (400) on creating mock data
	 */
	public static Result generateMockData()
	{
		// status to be returned
		Status methodStatus = null;

		try
		{
			// generates all mock data
			//generateAllMock();

			// in case mock data has been generated successfully
			methodStatus = ok();
		}
		catch (Exception e)
		{
			// in case of failure in generating mock data
			methodStatus = badRequest();
		}

		return methodStatus;
	}
	
	/**
	 * 
	 * @param jsonRequest
	 * @return
	 * @throws BusinessException
	 */
	private static Product extractProductFromJson(JsonNode jsonRequest) throws BusinessException
	{
		// break all pieces from the json in oder to initialize a rules object
		String name = jsonRequest.findPath("name").textValue();
		String description = jsonRequest.findPath("description").textValue();
		Boolean available = jsonRequest.findPath("available").booleanValue();
		Long id = jsonRequest.findPath("id").longValue();
		double price = jsonRequest.findPath("price").doubleValue();
		
		Product product = new Product();
		product.setAvailable(available);
		product.setDescription(description);
		product.setId(id);
		product.setName(name);
		product.setPrice(price);
		
		
		return product;
	}

}
