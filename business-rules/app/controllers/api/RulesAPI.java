package controllers.api;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

import exception.BusinessException;
import exception.RulesEngineException;
import mechanism.drools.RulesEngine;
import model.services.RuleServices;
import model.vo.Rule;
import play.mvc.Controller;
import play.mvc.Result;
import util.RestAPIResponseHandler;

public class RulesAPI extends Controller
{

	/**
	 * Method responsible for creating a business rule between two component options
	 * 
	 * @return response with HTTP status code indicating success (200) or failure (400) on creating the rule
	 */
	public static Result create()
	{
		// the status to be returned
		Status methodStatus = null;

		try
		{
			// retrieves the Json from the request
			JsonNode jsonRequest = request().body().asJson();

			// extracts the Rule from the Json
			Rule ruleToBeCreated = extractRuleFromJson(jsonRequest);

			// initiates RuleServices and create the rule
			RuleServices ruleServices = new RuleServices();
			ruleServices.createRule(ruleToBeCreated);

			// status for success on creating the rule
			methodStatus = ok("Success on creating the business rule");
		}
		catch (RulesEngineException ree)
		{
			// in case of failure related to rules, return an error status
			methodStatus = badRequest("Server encountered internal problems with rules.");
		}
		catch (Exception e)
		{
			// in case of failure, return an error status
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}

	public static Result getRuleById(Long id) {
		
		// the status to be returned
		Status methodStatus = null;

		try
		{
			// initiates the rules service and retrieves the list of all rules
			RuleServices services = new RuleServices();
			Rule rule = services.getRuleById(id);

			if (rule != null)
			{
				// status for success on finding a list of all rules
				methodStatus = ok(RestAPIResponseHandler.createJsonForBusinessRule(rule));
			}
			else
			{
				// status for finding no rules
				methodStatus = badRequest("No Business Rule found.");
			}
		}
		catch (Exception e)
		{
			// in case of failure, return an error status
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}
	/**
	 * Method responsible for listing all business rules
	 * 
	 * @return response with a list of compatibility rules as JSON object
	 */
	public static Result listAllRules()
	{
		// the status to be returned
		Status methodStatus = null;

		try
		{
			// initiates the rules service and retrieves the list of all rules
			RuleServices services = new RuleServices();
			List<Rule> list = services.getAllRules();

			if (list != null)
			{
				// status for success on finding a list of all rules
				methodStatus = ok(RestAPIResponseHandler.createJsonForBusinessRuleList(list));
			}
			else
			{
				// status for finding no rules
				methodStatus = badRequest("No Business Rules found.");
			}
		}
		catch (Exception e)
		{
			// in case of failure, return an error status
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}

	/**
	 * Method responsible for deleting a rule
	 * 
	 * @param ruleId id of the rule to be deleted
	 * @return response with HTTP status code for success (200) or failure (400) on deleting the rule
	 */
	public static Result delete(Long ruleId)
	{
		// the status to be returned
		Status methodStatus = null;

		try
		{
			// initiates the rules service and retrieves the list of all rules
			RuleServices services = new RuleServices();
			services.deleteRule(ruleId);

			// status for success on deleting the rule
			methodStatus = ok("Success on deleting the rule.");
		}
		catch (Exception e)
		{
			// in case of failure, return an error status
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}

	/**
	 * Method responsible for updating a rule
	 * 
	 * @param ruleId rule identifier
	 * @return response with HTTP status code for success (200) or failure (400) on updating the rule
	 */
	public static Result update(Long ruleId)
	{
		// status to be returned after operation
		Status methodStatus = null;

		try
		{
			// getting json from request and break all pieces in oder to initialize a rules object
			JsonNode jsonRequest = request().body().asJson();

			// extract the rules object to be updated from the json
			Rule ruleToUpdate = extractRuleFromJson(jsonRequest);

			// sets the ID of the rule to be updated
			ruleToUpdate.setId(ruleId);

			// try to update rule
			RuleServices rulesServices = new RuleServices();
			rulesServices.updateRule(ruleToUpdate);

			// if rule is successfully updated, initialize status with ok
			methodStatus = ok("Success on updating the business rule");
		}
		catch (Exception e)
		{
			// in case of failure, return an error status
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}

	/**
	 * Method responsible for validating a business rule syntax
	 * 
	 * @return response with HTTP status code for success (200) or failure (400) with a message on validating the rule
	 */
	public static Result validateRule()
	{
		// status to be returned after operation
		Status methodStatus = null;

		try
		{
			// extract the rules object to be validated from the json
			JsonNode jsonRequest = request().body().asJson();
			Rule ruleToValidate = extractRuleFromJson(jsonRequest);

			// try to validate the rule
			RulesEngine.getInstance().validateRule(ruleToValidate);

			// in case of successful validation, status is ok
			methodStatus = ok("Success validating the business rule");
		}
		catch (RulesEngineException e)
		{
			// in case of failure on the rule validation
			methodStatus = badRequest("Failure validating the business rule: " + e.getMessage());
		}
		catch (Exception e)
		{
			// in case of failure, return an error status
			methodStatus = badRequest("Server encountered internal problems.");
		}

		return methodStatus;
	}

	/**
	 * 
	 * @param jsonRequest
	 * @return
	 * @throws BusinessException
	 */
	private static Rule extractRuleFromJson(JsonNode jsonRequest) throws BusinessException
	{
		// break all pieces from the json in oder to initialize a rules object
		String name = jsonRequest.findPath("name").textValue();
		String dslr = jsonRequest.findPath("dslr").textValue();

		// rules object to be returned
		Rule ruleToBeReturned = new Rule();
		ruleToBeReturned.setName(name);
		ruleToBeReturned.setDslr(dslr);

		// returns the rule extracted from the json
		return ruleToBeReturned;
	}
}
