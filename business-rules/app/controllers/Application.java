package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import exception.BusinessException;

public class Application extends Controller
{

	public static Result index() throws BusinessException, Exception
	{
		return ok("Welcome Business Rules with Drools");
	}
}
