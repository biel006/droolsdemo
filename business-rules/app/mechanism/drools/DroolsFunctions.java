package mechanism.drools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * This class is responsible for manage the drools function
 * 
 */
public class DroolsFunctions {
	
	/**
	 * Function responsible for breaking a csv list of string values and separating them in a list of strings
	 * 
	 * @param csvListOfValues csv values to be returned as a list
	 * @return The @List<String> with the separated csv values
	 */
	public List<String> getListOfValuesFromCsv(String csvListOfValues){
		
		List<String> values = new ArrayList<>();
		List<String> trimmedValues = new ArrayList<>();
		
		// check if the list contains comma and if it does, separate the values
		if (csvListOfValues.contains(",")) {
			values = new ArrayList<String>(Arrays.asList(csvListOfValues.trim().split(",")));
		}
		else{
			values = new ArrayList<String>(Arrays.asList(csvListOfValues.trim().split(" ")));
		}

		for (String value : values) {
			trimmedValues.add(value.trim());
		}
		
		return trimmedValues;
	}
	
	/**
	 * Function responsible for checking if the given identifier
	 * is present in the csv list of identifiers passed from the business rule
	 * 
	 * @param ruleIdentifier @String that identifies the object to be matched
	 * @param csvListOfProductIdentifiers csv list of identifiers to be matched
	 * @return @Boolean indicating if the identifier is present in the csv list
	 */
	public Boolean isIdentifierPresent(String ruleIdentifier, String csvListOfProductIdentifiers){
		
		Boolean present = false;
		
		for(String identifier : getListOfValuesFromCsv(csvListOfProductIdentifiers)){
			if(ruleIdentifier.equals(identifier)){
				present = true;
			}
		}
		
		return present;
	}
}
