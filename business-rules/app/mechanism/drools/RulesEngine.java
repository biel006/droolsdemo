package mechanism.drools;

import static java.lang.String.format;
import static views.txt.ProductRulesTemplate.render;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.command.Command;
import org.drools.command.CommandFactory;
import org.drools.definition.KnowledgePackage;
import org.drools.io.ResourceFactory;
import org.drools.runtime.ExecutionResults;
import org.drools.runtime.StatelessKnowledgeSession;

import exception.BusinessException;
import exception.RulesEngineException;
import model.services.RuleServices;
import model.vo.Rule;
import play.Logger;
import play.mvc.Controller;


/**
 * This class is responsible for validate and apply rules created by the Mercury
 * administrators.
 * 
 * @author R&D Team - Eldorado Institute
 * @usedBy M4 Administration Console
 */
public class RulesEngine {
	
	private static RulesEngine instance;

	private KnowledgeBase knowledgeBase;

	private final RuleServices rulesService;

	/**
	 * Method responsible for returning the singleton RulesEngine instance.
	 *
	 * @return RulesEngine instance
	 * @throws RulesEngineException
	 */
	public static RulesEngine getInstance() throws RulesEngineException {
	    synchronized (RulesEngine.class) {
	      if (instance == null) {
	        instance = new RulesEngine();
	      }
	    }
		return instance;
	}

	/**
	 * Private constructor for Singleton purposes.
	 *
	 * @throws RulesEngineException
	 *             the rules engine exception
	 */
	private RulesEngine() throws RulesEngineException {
		this(new RuleServices());
	}
	
	/**
	 * Constructor at the package level for testing purposes
	 * 
	 * @param rulesService
	 * @throws RulesEngineException
	 */
	public RulesEngine(RuleServices rulesService) throws RulesEngineException {
		this.rulesService = rulesService;
		buildKnowledgeBase();
	}

	/**
	 * Method responsible for applying rules using a list of facts and getting an specific result
	 * 
	 * @param facts map containing all facts needed by the rules
	 * @param returnedObjectIdentification identification need by getting any results after applying rules
	 * @return object requested after applying rules
	 * @throws RulesEngineException exception in caso of any error applying the rules
	 */
	@SuppressWarnings("rawtypes")
	public Object applyRules(Map<String, Object> facts, String returnedObjectIdentification) throws RulesEngineException {
		// Creates the session to add the facts and apply the rules to the objects
		StatelessKnowledgeSession knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
		
		// building a list of facts to be applied in the rules
		List<Command> commands = buildCommandList(facts);

		// apply rules and get result
		Object returnedObject = null;
		try {
			ExecutionResults results = knowledgeSession.execute(CommandFactory.newBatchExecution(commands));
			returnedObject = results.getValue(returnedObjectIdentification);
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			throw new RulesEngineException(e.getMessage());
		}
		
		return returnedObject;
	}
	
	/**
	 * Method responsible for build a command list based on a map of facts
	 * 
	 * A command annotations is always appended with the command list
	 * 
	 * @param facts facts to build the list of commands
	 * @return a list of commands
	 */
	@SuppressWarnings("rawtypes")
	private List<Command> buildCommandList(Map<String, Object> facts) {
		List<Command> commands = new ArrayList<Command>();
		commands.add(CommandFactory.newQuery("annotations", "annotations"));
		for(Entry<String, Object> entry : facts.entrySet()) {
			commands.add(CommandFactory.newInsert(entry.getValue(), entry.getKey()));	
		}
		return commands;
	}

	/**
	 * Builds the knowledge base.
	 *
	 * @throws RulesEngineException
	 *             the rules engine exception
	 */
	public void buildKnowledgeBase() throws RulesEngineException {
		KnowledgeBuilder kbuilder = createKnowledgeBuilder();

		loadRules(kbuilder);

		// Initialize the KnowledgeBase that will be used to create the session
		knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();

		// Get the KnowledgePackage collection from the builder that will be
		// used on the KnowledgBase
		Collection<KnowledgePackage> packages = kbuilder.getKnowledgePackages();
		knowledgeBase.addKnowledgePackages(packages);
	}

	/**
	 * Creates the knowledge builder.
	 *
	 * @return the knowledge builder
	 */
	private KnowledgeBuilder createKnowledgeBuilder() {
		KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		knowledgeBuilder.add(ResourceFactory.newClassPathResource("mechanism/drools/MapDroolsRuleLanguage.dsl"),
				ResourceType.DSL);
		return knowledgeBuilder;
	}

	/**
	 * Load the existing rules.
	 *
	 * @param knowledgeBuilder
	 *            the {@link KnowledgeBuilder}
	 */
	private void loadRules(KnowledgeBuilder knowledgeBuilder) {
		List<Rule> rules = getAllRules();

		if (!rules.isEmpty()) {
			String expandedRules = render(rules).body();
			byte[] ruleByteRepresentation = expandedRules.getBytes();
			knowledgeBuilder.add(ResourceFactory.newByteArrayResource(ruleByteRepresentation), ResourceType.DSLR);
			if (knowledgeBuilder.hasErrors()) {
				throw new RulesEngineException(format("Invalid rule syntax: %n %s", knowledgeBuilder.getErrors()));
			}
		}
	}

	/**
	 * Gets the all rules.
	 *
	 * @return the all rules
	 */
	private List<Rule> getAllRules() {
		List<Rule> rules = new ArrayList<>();
		try {
			rules = rulesService.getAllRules();
		} catch (BusinessException e) {
			throw new RulesEngineException(Controller.internalServerError(e.getMessage()), e);
		}
		return rules;
	}

	/**
	 * Method responsible for verifying the (syntax, rule name) validity of a
	 * rule.
	 *
	 * @param rule
	 *            BusinessRule object to be validated
	 * @return boolean representing the verification result
	 * @throws RulesEngineException
	 *             throws exception in case of any problems regarding Drools
	 */
	public boolean validateRule(Rule ruleToBeValidate) throws RulesEngineException {
		// Create new knowledgeBuilder
		KnowledgeBuilder knowledgeBuilder = createKnowledgeBuilder();

		try {
			// Get all rules
			List<Rule> rules = rulesService.getAllRules();
			
			// It is necessary to remove the existing rule for an update
			removeExistingRuleForUpdate(ruleToBeValidate.getId(), rules);

			// Add rule to be validate in list of the all rules,
			// it is important validate rule with the other rules for checking
			// conflicts that cause errors
			rules.add(ruleToBeValidate);

			// Add rules in knowledge for validating
			addRuleToKnowledgeBuilder(knowledgeBuilder, rules);

			return true;
		} catch (BusinessException e) {
			throw new RulesEngineException(Controller.internalServerError(e.getMessage()), e);
		}
	}

	/**
	 * Verify if the validation is to create rule or update rule, whether rule
	 * has id, then the validation is to update
	 * 
	 * @param ruleId
	 * @param rules
	 */
	private void removeExistingRuleForUpdate(Long ruleId, List<Rule> rules) {
		if (ruleId != null) {
			rules.removeIf(r -> r.getId().equals(ruleId));
		}
	}

	/**
	 * Render all rules in one String using template and adds the rule to
	 * knowledge builder.
	 *
	 * @param knowledgeBuilder
	 *            the knowledge builder
	 * @param rules
	 *            the rules
	 */
	private void addRuleToKnowledgeBuilder(KnowledgeBuilder knowledgeBuilder, List<Rule> rules) {
		String expandedRule = render(rules).body();
		byte[] ruleByteRepresentation = expandedRule.getBytes();
		knowledgeBuilder.add(ResourceFactory.newByteArrayResource(ruleByteRepresentation), ResourceType.DSLR);
		if (knowledgeBuilder.hasErrors()) {
			throw new RulesEngineException(format("Invalid rule - reason: %n %s", knowledgeBuilder.getErrors()));
		}
	}
}
