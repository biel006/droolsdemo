# Comment: DSL DEMO

#Product
[condition][][Tt]here is a [Pp]roduct=$product:Product();
[consequence][][Cc]alculate a {percentage} percent discount=$product.setPrice($product.getPrice() * (1 - 0.01 * {percentage}));
[consequence][][Cc]alculate a {percentage}% discount over product=$product.setPrice($product.getPrice() * (1 - 0.01 * {percentage}));
[consequence][][Pp]roduct [Aa]vailable=$product.setAvailable(true);
[consequence][][Pp]roduct [Nn]ot [Aa]vailable=$product.setAvailable(false);


#Operators
[condition][]is less than or equal to=<=
[condition][]is less than=<
[condition][]is greater than or equal to=>=
[condition][]is greater than=>
[condition][]is equal to===
[condition][]equals===

#Abstract Language
#[condition][]- {field:\w*} {operator} {value:\d*}={field} {operator} {value}
#[condition][][Tt]here is an? {entity:\w+}=${entity!lc}: {entity!ucfirst}()
#[consequence][]Calculate a {percentage}%? discount over product = ${entity!lc}.setPrice(${entity!lc}.getPrice() * (1 - 0.01 * {percentage}));
#[consequence][]{entity:\w+} [Aa]vailable=${entity!lc}.setAvailable(true);
#[consequence][]{entity:\w+} [Nn]ot [Aa]vailable=${entity!lc}.setAvailable(false);
