name := """business-rules"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "org.drools" % "drools-compiler" % "5.6.0.Final",
  "org.drools" % "drools-core" % "5.6.0.Final",
  "org.drools" % "knowledge-api" % "5.6.0.Final",
  "com.google.code.gson" % "gson" % "2.3.1",
  "org.powermock" %  "powermock-module-junit4" 	% "1.6.0",
  "org.powermock" %  "powermock-api-mockito" 	% "1.6.0",
  "junit" % "junit" % "4.11" % "test"
)
