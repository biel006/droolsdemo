package mechanism.drools;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import exception.BusinessException;
import model.services.RuleServices;
import model.vo.Product;
import model.vo.Rule;

/**
 * These are the unit tests for the Product Availability Rule
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class ProductAvailabilityRuleTest {

	@Mock
	private RuleServices rulesService;

	@Mock
	private RulesEngine rulesEngine;
	
	@Mock
	private RulesEngineDataBuilder rulesEngineTestDataBuilder;

	@Before
	public void before() throws BusinessException, IOException {
		rulesEngineTestDataBuilder = new RulesEngineDataBuilder();
		List<Rule> rules = new ArrayList<>();
		rules.add(rulesEngineTestDataBuilder.buildRules("ProductAvailabilityRules.drl"));
		when(rulesService.getAllRules()).thenReturn(rules);
		rulesEngine = new RulesEngine(rulesService);
	}

	@Test
	public void applyRuleToEnableSingleProduct() {
		Product product = buildProductWithAvailabilityActive();
		
		HashMap<String, Object> facts = new HashMap<>();
		facts.put("product", product);
		
		product = (Product) rulesEngine.applyRules(facts, "product");
		
		assertThat(product.isAvailable()).isEqualTo(true);
	}

	private Product buildProductWithAvailabilityActive() {
		Product product = new Product();
		product.setName("Product New");
		product.setAvailable(true);
		return product;
	}
}
