package mechanism.drools;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import exception.BusinessException;
import exception.RulesEngineException;
import model.dao.RuleDAO;
import model.services.RuleServices;
import model.vo.Product;
import model.vo.Rule;

/**
 * These are the unit tests for the Rule Engine
 */
@RunWith(MockitoJUnitRunner.class)
public class RulesEngineTest {

	@Mock
	private RuleDAO ruleDAO;
	
	@Mock
	private RuleServices rulesService;

	private RulesEngine rulesEngine;

	private RulesEngineDataBuilder rulesEngineTestDataBuilder;
	
	@Before
	public void before() throws BusinessException, IOException {
		rulesEngineTestDataBuilder = new RulesEngineDataBuilder();
		List<Rule> rules = new ArrayList<>();
		rules.add(rulesEngineTestDataBuilder.buildRules("/mechanism/drools/RuleSample.drl"));	
		when(rulesService.getAllRules()).thenReturn(rules);
		rulesEngine = new RulesEngine(rulesService);
	}
	
	@Test
	public void applyRulesActivateProduct()  {
		Product product = buildDisabledProduct();
		
		HashMap<String, Object> facts = new HashMap<>();
		facts.put("product", product);
		
		product = (Product) rulesEngine.applyRules(facts, "product");
		
		assertThat(product.isAvailable()).isEqualTo(true);
	}
	
	@Test
	public void validate() throws IOException {
		Rule validRule = rulesEngineTestDataBuilder.buildRules("/mechanism/drools/ValidRuleSample.drl");
		boolean isValid = rulesEngine.validateRule(validRule);
		assertThat(isValid).isTrue();
	}
	
	@Test(expected = RulesEngineException.class)
	public void validateThrowsRuleEngineExceptionSintaxError() throws IOException {
		Rule invalidRule = rulesEngineTestDataBuilder.buildRules("/mechanism/drools/InvalidRuleSample.drl");
		rulesEngine.validateRule(invalidRule);
	}
	
	@Test(expected = RulesEngineException.class)
	public void validateThrowsRuleEngineExceptionSameName() throws IOException {
		Rule validRuleWithSameName = rulesEngineTestDataBuilder.buildRules("/mechanism/drools/RuleSample.drl");
		rulesEngine.validateRule(validRuleWithSameName);
	}
	
	private Product buildDisabledProduct() {
		Product product = new Product();
		product.setName("Product New");
		product.setAvailable(true);
		return product;
	}
}
