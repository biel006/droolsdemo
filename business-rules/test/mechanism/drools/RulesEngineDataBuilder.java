package mechanism.drools;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import model.vo.Rule;

/**
 * This class is responsible for build rules to test
 */
public class RulesEngineDataBuilder {

	public Rule buildRules(String ruleFileName) throws IOException {

		File file = new File(getClass().getResource(ruleFileName).getFile());
		System.out.println(file.getAbsolutePath());
			
		Rule rule = new Rule();

		String rulesFileAsString = FileUtils.readFileToString(file);
		rule.setDslr(rulesFileAsString);

		return rule;
	}

}
