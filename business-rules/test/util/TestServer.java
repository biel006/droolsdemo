package util;

import static play.test.Helpers.testServer;
import play.libs.ws.WS;
import play.libs.ws.WSRequestHolder;
import play.libs.ws.WSResponse;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.config.ServerConfig;
import com.avaje.ebean.config.dbplatform.H2Platform;
import com.avaje.ebeaninternal.api.SpiEbeanServer;
import com.avaje.ebeaninternal.server.ddl.DdlGenerator;

public class TestServer
{
	// Constants
	public final static String TEST_SERVER = "http://localhost:3333/";
	public final static int TEST_SERVER_PORT = 3333;
	public final static String SECURITY_KEY = "86a7831fc40e61a116997970263ffda2";
	public final static int TIMEOUT = 3000000;

	// test server instance
	private static play.test.TestServer _testServer = null;

	/**
	 * Method responsible for starting a new test server
	 */
	public static void initialize()
	{
		if (_testServer == null || !TestServer.isRunning())
		{
			_testServer = testServer(TEST_SERVER_PORT);
			_testServer.start();
		}
	}

	/**
	 * Method responsible for shutting down test server
	 */
	public static void shutdown()
	{
		_testServer.stop();
	}

	/**
	 * Method responsible for checking if test server is running
	 * 
	 * @return flag indicating if server is running or not
	 */
	public static boolean isRunning()
	{

		return _testServer != null && (WS.url(TEST_SERVER).get().get(TIMEOUT).getStatus() == 200);

	}

	/**
	 * Method responsible for performing a POST request with a XML
	 * 
	 * @param apiEndPoint api end point which will be reached by this request
	 * @param xmlRequestBody xml to be sent as request body
	 * @return request's response
	 */
	public static WSResponse performPostWithXML(String apiEndPoint, String xmlRequestBody)
	{
		// perform request
		WSRequestHolder wsRequestHolder = WS.url(TEST_SERVER + apiEndPoint);
		wsRequestHolder.setContentType("application/xml");
		wsRequestHolder.setHeader("Authorization", SECURITY_KEY);
		WSResponse response = wsRequestHolder.post(xmlRequestBody).get(TIMEOUT);
		return response;
	}
	
	/**
	 * Method responsible for performing a PUT request with a XML
	 * 
	 * @param apiEndPoint api end point which will be reached by this request
	 * @param xmlRequestBody xml to be sent as request body
	 * @return request's response
	 */
	public static WSResponse performPutWithXML(String apiEndPoint, String xmlRequestBody)
	{
		// perform request
		WSRequestHolder wsRequestHolder = WS.url(TEST_SERVER + apiEndPoint);
		wsRequestHolder.setContentType("application/xml");
		wsRequestHolder.setHeader("Authorization", SECURITY_KEY);
		WSResponse response = wsRequestHolder.post(xmlRequestBody).get(TIMEOUT);
		return response;
	}
	
	/**
	 * Method responsible for performing a PUT request with a JSON
	 * 
	 * @param apiEndPoint api end point which will be reached by this request
	 * @param jsonRequestBody json to be sent as request body
	 * @return request's response
	 */
	public static WSResponse performPutWithJSON(String apiEndPoint, String jsonRequestBody)
	{
		// perform request
		WSRequestHolder wsRequestHolder = WS.url(TEST_SERVER + apiEndPoint);
		wsRequestHolder.setContentType("application/json");
		wsRequestHolder.setHeader("Authorization", SECURITY_KEY);
		WSResponse response = wsRequestHolder.post(jsonRequestBody).get(TIMEOUT);
		return response;
	}

	/**
	 * Method responsible for performing a GET request
	 * 
	 * @return request's response
	 */
	public static WSResponse performGet(String apiEndPoint)
	{
		// perform request
		WSRequestHolder wsRequestHolder = WS.url(TEST_SERVER + apiEndPoint);
		wsRequestHolder.setHeader("Authorization", SECURITY_KEY);
		WSResponse response = wsRequestHolder.get().get(TIMEOUT);
		return response;
	}
	
	/**
	 * Method responsible for performing a DELETE request
	 * 
	 * @return request's response
	 */
	public static WSResponse performDelete(String apiEndPoint)
	{
		// perform request
		WSRequestHolder wsRequestHolder = WS.url(TEST_SERVER + apiEndPoint);
		wsRequestHolder.setHeader("Authorization", SECURITY_KEY);
		WSResponse response = wsRequestHolder.delete().get(TIMEOUT);
		return response;
	}

	/**
	 * Method responsible for recreating database
	 */
	public static void recreateDatabase()
	{
		// setup ddl generator
		EbeanServer server = Ebean.getServer("default");
		ServerConfig config = new ServerConfig();
		DdlGenerator ddlGenerator = new DdlGenerator();
		ddlGenerator.setup((SpiEbeanServer) server, new H2Platform(), config);

		// execute drop script
		ddlGenerator.runScript(false, ddlGenerator.generateDropDdl());

		// execute create script
		ddlGenerator.runScript(false, ddlGenerator.generateCreateDdl());
		
		// execute insert script
		ddlGenerator.runScript(false, "insert into configuration(key, value) values ('PIM-SYSTEM-API', 'mechanism.pimsystem.MockPIMSystem');");
	}
}
