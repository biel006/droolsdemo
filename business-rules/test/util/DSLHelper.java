package util;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;


public class DSLHelper {
	
	public static String getValidDSL(){
    	Path path = FileSystems.getDefault().getPath("test/resources", "SampleDSLR.dslr");
    	byte[] rulesBytesArray = null;
    			
    	try
        {
	        rulesBytesArray = java.nio.file.Files.readAllBytes(path);
        }
        catch (IOException e)
        {
	        e.printStackTrace();
        }
    	
		return new String(rulesBytesArray);
	}
	
	public static String getInvalidDSL(){
		
		return "Invalid DSL string";
	}
	
	public static String getDSLForProductWithDiscountOnlyWeekends()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("rule \"Discount 10% Weekend \"\n");
		sb.append("when\n");
		sb.append("There is a Product\n");
		sb.append("In this context\n");
		sb.append("On weekends\n");
		sb.append("then\n");
		sb.append("Calculate a 10% discount over product\n");
		sb.append("end");
		return sb.toString();
	}
}
