(function () {
    'use strict';

    /* 
    * Service responsible for providing services for stores
    * @param {!m4admin.controllers.M4CEManager} M4CEManager mechanism providing methods to access M4Ce endpoints
    * @param {!angular.$filter} $filter Angular $filter service
    */
    angular.module('demo.core.store').service('StoreServices', StoreServices);

    /* Dependency Injection */
    StoreServices.$inject = ['RESTServiceManager', '$filter', '$q'];
    
    /**
     * Service responsible for dealing with stores 
     */
    function StoreServices(RESTServiceManager, $filter, $q) {
        /**
         * Get all stores
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.getAllProducts = function () {
            return RESTServiceManager.performRequest('GET', 'store', undefined, undefined);
        };
    }
})();

