(function(){
    'use strict';
    
    /**
     * Controller responsible to handle functionalites related to the products search UI
     * 
     * @param {!angular.Scope} $scope Angular scope service
     * @param {Angular-UI-Modal} $modal angular-ui wrapper for bootstrap modal
     * @param {AngularJS-Toaster} toaster Angular thirty party module
     * @param {!angular.$filter} $filter Angular $filter service
     * @param {!angular.$location} $location Angular $location wrapper
     * @param {!demo.core.products.ProductServices} ProductServices application product services
     * @param {!demo.constants.PaginationConstants} PaginationConstants which contains the application's constants
     */
    angular.module('demo.core.products').controller('SearchProductsController', SearchProductsController);
    
    // Dependency Injection
    SearchProductsController.$inject = ['$scope', '$modal', 'toaster','$filter','$location', 'ProductServices', 'PaginationConstants'];
   
    // Controller Implementation
    function SearchProductsController($scope, $modal, toaster, $filter, $location, ProductServices,  PaginationConstants){
        // initialize all variables need to put this working
        $scope.productList = [];
        $scope.promise = undefined;
        
        // Pagination and search variables
        $scope.preparedProductList = [];
        $scope.productNameToSearch = '';
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = PaginationConstants.ITEMS_PER_PAGE;
        $scope.maxSize = PaginationConstants.MAX_SIZE;
        
        /**
         * Function responsible for preparing the product list to show it in the paginated table
         * It will - filter the productList by the search field
         * 		   - order the result of the filter by partnerName
         *         - return the sliced result to show it paginated
         * @returns {list} filtered and paginated list of products
         */
        $scope.getPreparedProductList = function(){
            $scope.preparedProductList = $filter('filter')($scope.productList, {name: $scope.productNameToSearch});
            $scope.preparedProductList = $filter('orderBy')($scope.preparedProductList, 'name');
    
            $scope.totalItems = $scope.preparedProductList.length;  
                
            return $scope.preparedProductList.slice(($scope.currentPage-1) * $scope.itemsPerPage, (($scope.currentPage-1) * $scope.itemsPerPage) + $scope.itemsPerPage);
        };

        /**
         * Function responsible for getting all products
         */
        $scope.getAllProducts = function() {
            // call the get all products method from service
            $scope.promise = ProductServices.getAllProducts();

            //callback in case of success to retrieve the product list
            var successCallback = function(response) {
                // get answer from response
                $scope.productList = response.data;
            };

            //callback in case of failure to fetch the list of products
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('searchProducts.errorMessage.loadProductInformationFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
        };

        /**
         * Function responsible for redirecting user to edit a product
         * @param {String} id product identifier to be edited
         */
        $scope.editProduct = function (id) {
            // redirect user to product edition
            $location.path( '/products/' + id + '/manage');
        };

        /**
         * Function responsible for deleting a given product
         * @param {String} id product identifier to be edited
         */
        $scope.deleteProduct = function (id) {
            // call the get all products method from service
            $scope.promise = ProductServices.deleteProduct(id);

            //callback in case of success to retrieve the product list
            var successCallback = function(response) {
                // removing item from list
                $scope.productList = $scope.productList.filter(i => i.id !== id);
            };

            //callback in case of failure to fetch the list of products
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('searchProducts.errorMessage.deleteFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
        };

        /**
         * Function responsible for redirect the user to Creation page
         */
        $scope.goToProductCreation = function () {
            $location.path('/products/create');
        };
    }  
})();

