(function(){
  'use strict';
  
  /**
   * Controller responsible for manipulating maintain context view
   *
   * @param {!angular.Scope} $scope Angular scope service
   * @param {Angular-UI-Modal} $modal angular-ui wrapper for bootstrap modal
   * @param {!angular.Location} $location Angular $location wrapper
   * @param {!angular.RouteParams} $routeParams Angular routeParams service
   * @param {AngularJS-Toaster} toaster Angular thirty party module
   * @param {!angular.Filter} $filter Angular filter service
   * @param {!m4admin.controllers.ContextServices} ContextServices context services
   */
  angular.module('demo.core.products').controller('ProductDetailsController', ProductDetailsController);
    
  // Dependency Injection
  ProductDetailsController.$inject = ['$scope', '$modal', '$location', '$routeParams', 'toaster', '$filter', 'ProductServices'];
    
  // Controller implementation  
  function ProductDetailsController($scope, $modal, $location, $routeParams, toaster, $filter, ProductServices) {
      $scope.operationType = 'Create';

      $scope.product = {
          name: undefined,
          price: undefined,
          available: undefined
      };
      $scope.productTypes = [];

      $scope.initPage = function() {
          $scope.productTypes = ProductServices.getProductTypes();
          var id = $routeParams.id;

          if(id) {
              $scope.getProductInfo(id);
          }
      }

      $scope.getProductInfo = function(id) {
            // call the get all products method from service
            $scope.promise = ProductServices.getProductById(id);
            
            //callback in case of success to retrieve the product list
            var successCallback = function(response) {
                // get answer from response
                $scope.product = response.data;
                $scope.operationType = response.data.name;
            };

            //callback in case of failure to fetch the list of products
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('productDetails.errorMessage.loadProductInformationFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
      };

      $scope.backToProducts = function() {
          $location.url('products');
      };

      $scope.saveProduct = function() {
            $scope.promise = ProductServices.saveProduct($scope.product);
            
            //callback in case of success to retrieve the product list
            var successCallback = function(response) {
                toaster.pop('success', '', $filter('translate')('productDetails.successMessage.saveProductInformationSuccess'));
                $location.url('products');
            };

            //callback in case of failure to fetch the list of products
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('productDetails.errorMessage.saveProductInformationFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
      };
  }
})();