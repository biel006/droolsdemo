(function () {
    'use strict';

    /* 
    * Service responsible for providing services for products
    * @param {!m4admin.controllers.M4CEManager} M4CEManager mechanism providing methods to access M4Ce endpoints
    * @param {!angular.$filter} $filter Angular $filter service
    */
    angular.module('demo.core.products').service('ProductServices', ProductServices);

    /* Dependency Injection */
    ProductServices.$inject = ['RESTServiceManager', '$filter', '$q'];
    
    /**
     * Service responsible for dealing with products 
     */
    function ProductServices(RESTServiceManager, $filter, $q) {
        /**
         * Get all products
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.getAllProducts = function () {
            return RESTServiceManager.performRequest('GET', 'products', undefined, undefined);
        };
        
        /**
         * Get a products by its identifier
         * @param {integer} product's identifier
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.getProductById = function (id) {
            return RESTServiceManager.performRequest('GET', 'products', [id], undefined);
        };

        /**
         * Delete a products by its identifier
         * @param {integer} product's identifier
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.deleteProduct = function (id) {
            return RESTServiceManager.performRequest('DELETE', 'products', [id], undefined);
        };

        /**
         * Function responsible for saving a given product
         * @param {object} product to be saved
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.saveProduct = function (product) {
            if(product.id) {
                return RESTServiceManager.performRequest('PUT', 'products', [product.id], product);
            } else {
                return RESTServiceManager.performRequest('POST', 'products', undefined, product);
            }
        };

        /**
         * Get an array with the product types
         * @returns {Array} array of product types
         */
        this.getProductTypes = function () {
            return [{
                id: 0,
                value: 'Type A'
            }, {
                id: 1,
                value: 'Type B'
            }, {
                id: 2,
                value: 'Type C'
            }];
        };
    }
})();

