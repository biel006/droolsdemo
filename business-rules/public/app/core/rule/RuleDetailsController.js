(function(){
  'use strict';
  
  /**
   * Controller responsible for manipulating maintain context view
   *
   * @param {!angular.Scope} $scope Angular scope service
   * @param {Angular-UI-Modal} $modal angular-ui wrapper for bootstrap modal
   * @param {!angular.Location} $location Angular $location wrapper
   * @param {!angular.RouteParams} $routeParams Angular routeParams service
   * @param {AngularJS-Toaster} toaster Angular thirty party module
   * @param {!angular.Filter} $filter Angular filter service
   * @param {!m4admin.controllers.ContextServices} ContextServices context services
   */
  angular.module('demo.core.rules').controller('RuleDetailsController', RuleDetailsController);
    
  // Dependency Injection
  RuleDetailsController.$inject = ['$scope', '$modal', '$location', '$routeParams', 'toaster', '$filter', 'RuleServices'];
    
  // Controller implementation  
  function RuleDetailsController($scope, $modal, $location, $routeParams, toaster, $filter, RuleServices) {
      $scope.operationType = 'Create';

      $scope.rule = {
          name: undefined,
          content: undefined
      };

      $scope.initPage = function() {
          var id = $routeParams.id;

          if(id) {
              $scope.getRuleInfo(id);
          }
      }

      $scope.getRuleInfo = function(id) {
            // call the get all rules method from service
            $scope.promise = RuleServices.getRuleById(id);
            
            //callback in case of success to retrieve the Rule list
            var successCallback = function(response) {
                // get answer from response
                $scope.rule = response.data;
                $scope.operationType = response.data.name;
            };

            //callback in case of failure to fetch the list of rules
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('ruleDetails.errorMessage.loadRuleInformationFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
      };

      $scope.backToRules = function() {
          $location.url('rules');
      };

      $scope.saveRule = function() {
            $scope.promise = RuleServices.saveRule($scope.rule);
            
            //callback in case of success to retrieve the Rule list
            var successCallback = function(response) {
                toaster.pop('success', '', $filter('translate')('ruleDetails.successMessage.saveRuleInformationSuccess'));
                $location.url('rules');
            };

            //callback in case of failure to fetch the list of rules
            var errorCallback = function(response) {
                if(response.status > 0){
                    toaster.pop('error', '', $filter('translate')('ruleDetails.errorMessage.saveRuleInformationFailure'));
                } else {
                    toaster.pop('error','', $filter('translate')('general.errorMessage.timeout'));
                }  
            };
        
            // Resolve promise
            $scope.promise.then(successCallback, errorCallback);
      };
  }
})();