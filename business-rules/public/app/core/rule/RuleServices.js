(function () {
    'use strict';

    /* 
    * Service responsible for providing services for rules
    * @param {!m4admin.controllers.M4CEManager} M4CEManager mechanism providing methods to access M4Ce endpoints
    * @param {!angular.$filter} $filter Angular $filter service
    */
    angular.module('demo.core.rules').service('RuleServices', RuleServices);

    /* Dependency Injection */
    RuleServices.$inject = ['RESTServiceManager', '$filter', '$q'];
    
    /**
     * Service responsible for dealing with rules 
     */
    function RuleServices(RESTServiceManager, $filter, $q) {
        /**
         * Get all rules
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.getAllRules = function () {
            return RESTServiceManager.performRequest('GET', 'rules', undefined, undefined);
        };
        
        /**
         * Get a rules by its identifier
         * @param {integer} rule's identifier
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.getRuleById = function (id) {
            return RESTServiceManager.performRequest('GET', 'rules', [id], undefined);
        };

        /**
         * Delete a rules by its identifier
         * @param {integer} rule's identifier
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.deleteRule = function (id) {
            return RESTServiceManager.performRequest('DELETE', 'rules', [id], undefined);
        };

        /**
         * Function responsible for saving a given rule
         * @param {object} rule to be saved
         * @returns {Promise} a promise with success and error methods using function(data, status, headers, config)
         */
        this.saveRule = function (rule) {
            if(rule.id) {
                return RESTServiceManager.performRequest('PUT', 'rules', [rule.id], rule);
            } else {
                return RESTServiceManager.performRequest('POST', 'rules', undefined, rule);
            }
        };

        this.encodeRuleText =  function(name, precondition, consequence){
            return "rule \"" + name + "\"\n" +
                "when\n" + precondition + "\n" +
                "then\n" + consequence + "\n" +
                "end";
        }
    }
})();

