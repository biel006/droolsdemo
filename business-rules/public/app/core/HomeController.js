(function(){
  'use strict';
  
  /**
   * Controller responsible for manipulating home view
   *
   * @param {!angular.Scope} $scope Angular scope service
   */
  angular.module('demo.core').controller('HomeController', HomeController);
    
    // Dependency Injection
    HomeController.$inject = ['SecurityServices', '$scope'];
    
    // Controller implementation
    function HomeController(SecurityServices, $scope) {

    }
})();
